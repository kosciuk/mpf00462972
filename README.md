# Archivos de la Causa MPF00462972

Estos son los archivos de la causa donde el Hospital Piñero dejo morir de hambre
a mi madre y de como el Ministerio Público Fiscal de CABA se encargo de encubrirlo.

Aunque hay 90 centros de acceso a la Justicia, tengo que andar tele-mendigando 
ayuda para resolver el misterio y quedarme tranquilo.

La intención de dejar los archivos es que sirvan para que tanto estudiantes de Medicina o 
Criminalística (o cualquier rama forense) hagan sus prácticas y de paso me puedan explicar 
como es que la perito llego a la conclusión de que se le daba de comer cuando:

- creo que faltan los informes y controles de enfermeria de la Unidad 1, supongo que debería haber ya que están los 
de la Unidad 4
- los médicos reclaman que el familiar no va a darle de comer ni a hidratarla (¿estaría sin suero?)
- ni se hace mención a la sonda nasogastrica (¿como se llegó a ese extremo¿ ¿era necesario habiendo familiar?

Referente a la HC en general me pregunto:

- ¿por qué al momento del ingreso ponen "indigente" e "inanición" cuando el ingreso fue debido a un problema de sobrepeso?

¿Es realmente necesario estudiar no se cuantos años para poder afirmar que tarde o temprano la gente se muere? Esa materia la 
podría hacer libre, incluso yo me animaría a afirmar que el afiliado a PAMI tiene más probabilidad de morir que otra persona que no tenga.

Se debe tener en cuenta que el único familiar fue expulsado el 25 de marzo [video](https://youtu.be/jzmWhk7Fs-Y) y la paciente fallece el 
28 de abril.

También puede servir a estudiantes de Derecho quienes pueden aprender:
- como encubrir una causa, cuando sean Fiscales o Jueces, o 
- como evitar que la encubran si son los que buscan la verdad.

Entre mis sospechas de encubrimiento están:

- la tardanza en intentar "allanar" (tiempo en que se habrían coordinado acciones con el hospital)
- el intento de allanar, ¿que sentido tiene allanar si el Hospital tuvo la historia clínica en su poder durante 50 dias?
- la declaración del médico que me expulso, Schinocca (comparenla con el video). Creo que el 
Fiscal aconsejo al médico que invente una persona para poder poner en su resolución que además de ser peligroso para mi madre, lo era tambien para terceros. Obviamente, nunca se intentó buscar la historia 
clínica de ese fantasma.
- solo se entrevisto al imputado y su superior y a un médico que dijo que daba remedios vencidos a mi mama... para que daría remedios vencidos teniendo los comprados un mes antes... eso se averigua facilmente en PAMI.
- no se me permitió querellar
- se usaron las mentiras de los médicos para impedirme ser querellante, y aun cuando parece que no era necesario, el Fiscal siguió insistiendo en eso, y tardó 10 días para leer el informe y aprobarme como querellante.
- no se hizo autopsia, ni hisopado, ni siquiera se pesó el cuerpo
- ¿si es verdad que la HC está incompleta y lo supieron el 18 de mayo, no pudieron hacer autopsia antes del 27 cuando salió de la morgue del Piñero?
- cuando se denunció por encubrimiento al Fiscal de esta causa, el Fiscal de Camara puso en su resolución que yo le daba de comer comida sacada de la basura a mi mama. Hay tanta impunidad que pueden encubrirse mintiendo.
- aún no se supo quien dió la orden de expulsarme, ya que él médico declaro que deje de ir por la cuarentena
- en la HC hay un reclamo en el libro de quejas, pero no pudieron encontrar la nota que deje en Dirección (no quiso el Fiscal adjuntar una prueba que demostraria que el Hospital estuvo al tanto de mi expulsión)

Si alguna institución desea usar esta causa para algún trabajo práctico y necesita que suba más videos o fotos, no dude en contactarse.

Con suerte, puede que haya más cosas en mi [página web](http://kosciuk.com.ar/margarita/justicia.html), aunque hace mal releer esto muy seguido.
